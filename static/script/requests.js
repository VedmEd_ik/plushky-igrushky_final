// Запит на сервер для отримання всіх товарів
class GetProductsFromServer {
	constructor(button, id) {
		this.button = button;
		this.id = id;
	};

	async getProductsFromServer() {
		const url = '../json/toys.json';

		let response = await fetch(url);
		if (response.ok) {
			let result = await response.json();
			loadProducts(result);
			this.button.classList.remove('hold');
			console.log(GetProductsFromServer.counter);
			GetProductsFromServer.counter++;
		} else {
			alert('Помилка');
		};
	};
}
GetProductsFromServer.counter = 0;

// Запит на сервер для отримання фото товару
async function getPhotoProduct(idProduct) {
	const url = '../json/photos.json';
	let response = await fetch(`${url}?idProduct=${idProduct}`, {
		method: 'GET',
	});
	if (response.ok) {
		let photos = await response.json();
		showProduct(photos, idProduct);
	}
}

// Запит для відправки на сервер замовлення
async function sendOrderToServer(data) {
	const url = '';

	const fetchData = {
		method: 'POST',
		body: data,
		headers: new Headers()
	}

	fetch(url, fetchData)
		.then(function (response) {
			if (response.ok) {
				thankForOrder();
			}
		});
}

// Запит для відправки на сервер нового товару з адмін сторінки
async function sendNewProductToServer(data) {
	const url = '/addtoy';

	const fetchData = {
		method: 'POST',
		body: data,
		headers: {
			'Content-Type': 'application/json'
		}
	}

	fetch(url, fetchData)
		.then(function (response) {
			if (response.ok) {
				alert("Нова іграшка додана на сайт");
			};
		});
}
