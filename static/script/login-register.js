document.addEventListener('DOMContentLoaded', () => {
	document.addEventListener('click', documentClick);

	// Делегування події "Клік"
	function documentClick(event) {
		const targetElement = event.target;

		// Клік по хрестику для закриття вікна
		if (targetElement.closest('.close') && targetElement.closest('.account')) {
			window.open('./index.html', "_self");
		}
		// Клік по посиланню "Ввійти"
		if (targetElement.closest('.account__login')) {
			event.preventDefault();
			document.querySelector('.account__login-form').style.display = 'flex';
			document.querySelector('.account__register-form').style.display = 'none';

		};
		// Клік по посиланню "Зареєструватися"
		if (targetElement.closest('.account__register')) {
			event.preventDefault();
			document.querySelector('.account__login-form').style.display = 'none';
			document.querySelector('.account__register-form').style.display = 'flex';
		};
		// Клік по кнопці "Вхід" у вікні входу
		if (targetElement.id === 'login') {
			event.preventDefault();
			const form = document.querySelector('#login-form');
			authorizationOnSite(getDataForm(form), '/loginuser');
		};
		// Клік по кнопці "Зареєструватися" у вікні реєстрації
		if (targetElement.id === 'register-form-submit') {
			event.preventDefault();
			const form = document.querySelector('#register-form');
			authorizationOnSite(getDataForm(form), '/newuser');
		};
	};

	// Функція отримання даних з форми
	function getDataForm(form) {
		const formData = new FormData(form);
		const values = Object.fromEntries(formData.entries());
		return JSON.stringify(values);
	}

	// Запит на сервер для авторизації на сайті
	async function authorizationOnSite(formData, url) {
		const fetchData = {
			method: 'POST',
			body: formData,
			headers: {
				'Content-Type': 'application/json'
			}
		};

		const response = await fetch(url, fetchData);

		if (response.ok) {
			const result = await response.json();
			console.log(result);
			validateLoginAndPassword(result);
		}
	}

	// Форма для входу
	// Функція порівняння введених логіна і пароля з тими, що знаходяться в базі даних
	function validateLoginAndPassword(data) {

		switch (data.description) {
			case "ADMIN": {
				window.open('./admin.html', "_self");
				break;
			};
			case "OK": {
				window.open('./index.html', "_self");
				break;
			};
			case "User does not exist": {
				addUncorrectClass(document.getElementById('login-form__login'));
				addUncorrectClass(document.getElementById('login-form__password'));
				alert("Користувача з такими даними не знайдено. Перевірте, будь ласка, введені логін та пароль!");
				break;
			};
			case "Wrong password": {
				addUncorrectClass(document.getElementById('login-form__password'));
				alert("Неправильний пароль!");
				break;
			};
			case "User already exists": {
				alert("Такий користувач вже зареєстрований.");
				break;
			};
		};
	};

	// Форма для реєстрації
	const allInputRegisterForm = document.querySelectorAll('.account__register-form input');
	allInputRegisterForm.forEach((input) => {
		input.addEventListener('input', validateRegistrationData);
	});

	// Функція валідації введених даних
	function validateRegistrationData() {
		const buttonSubmit = document.getElementById('register-form-submit');
		const name = document.getElementById('register-form__name');
		const login = document.getElementById('register-form__login');
		const password = document.getElementById('register-form__password');
		const passwordRepeat = document.getElementById('register-form__password-repeat');

		if (validateName(name) && validateEmail(login) && validatePassword(password, passwordRepeat)) {
			buttonSubmit.disabled = false;
		} else {
			buttonSubmit.disabled = true;
		};
	};

	// Валідація імені
	function validateName(nameInput) {
		if (nameInput.value) {
			removeUncorrectClass(nameInput);
			return true;
		} else {
			addUncorrectClass(nameInput);
			return false;
		};
	};

	// Валідація пошти
	function validateEmail(emailInput) {
		const regExp = new RegExp(/\S+@\S+\.\S+/);
		if (regExp.test(emailInput.value)) {
			removeUncorrectClass(emailInput);
			return true;
		} else {
			addUncorrectClass(emailInput);
			return false;
		};
	};

	// Валідація пароля
	function validatePassword(passwordInput, validePassword) {
		if (passwordInput.value === validePassword.value) {
			removeUncorrectClass(passwordInput);
			removeUncorrectClass(validePassword);
			return true;
		} else {
			addUncorrectClass(passwordInput);
			addUncorrectClass(validePassword);
			return false;
		};
	};

	// Функція додавання червоної рамки
	function addUncorrectClass(inputElement) {
		inputElement.classList.add('uncorrect');
	};

	// Функція видалення червоної рамки
	function removeUncorrectClass(inputElement) {
		if (inputElement.classList.contains('uncorrect')) {
			inputElement.classList.remove('uncorrect');
		};
	};

});

