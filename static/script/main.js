'use strict';

document.addEventListener("DOMContentLoaded", (event) => {

	// Обробка кліку на всій сторінкці
	document.addEventListener('click', documentClick);

	// Завантаження першої партії іграшок для головної сторінки
	getProducts(document.querySelector('.main__more-button'));

	// Наведення мишки на елемент меню
	$('.nav-menu__item').hover(function () {
		$(this).css('transform', 'scale(1.1)');
		$(this).css('cursor', 'pointer');
	},
		function () {
			$(this).css('transform', 'scale(1)');
		}
	);
})

// Делегування події "click"
function documentClick(event) {
	const targetElement = event.target;

	// Клік по кнопці "Показати більше"
	if (targetElement.classList.contains('main__more-button')) {
		event.preventDefault();
		getProducts(targetElement);
	};
	// Клік по елементі фільтру - категорії іграшок
	if (targetElement.closest('.nav-menu__item')) {
		event.preventDefault();
		filterProducts(targetElement);
	};
	// Клік по кнопці "Купити" на картці товару
	if (targetElement.classList.contains('product-card__button')) {
		event.preventDefault();
		const productId = targetElement.closest('.product-card').dataset.prodid;
		productInCart(productId, targetElement);
	};
	// Клік по кнопці "Купити" у картці відкритого товару
	if (targetElement.classList.contains('product__button')) {
		event.preventDefault();
		const productId = targetElement.closest('.product').dataset.prodid;
		productInCart(productId, targetElement);
	};
	// Клік поза межами розділу "Про мене" для його закриття
	if (!targetElement.closest('.for-me-wrapper')) {
		document.querySelector('.for-me-wrapper').style.display = 'none';
	};
	// Клік по посиланню "Про мене"
	if (targetElement.classList.contains('for-me-link')) {
		event.preventDefault();
		document.querySelector('.for-me-wrapper').style.display = 'block';
		document.querySelector('.for-me').classList.add('visible');
	};
	// Клік поза межами розділу "Контакти" для його закриття
	if (!targetElement.closest('.contacts-wrapper')) {
		document.querySelector('.contacts-wrapper').style.display = 'none';
	};
	// Клік по посиланню "Контакти"
	if (targetElement.classList.contains('contacts-link')) {
		event.preventDefault();
		document.querySelector('.contacts-wrapper').style.display = 'block';
		document.querySelector('.contacts').classList.add('visible');
	}

	if (targetElement.closest('.account__icon-link')) {
		event.preventDefault();
		window.open('./login.html', "_self");
	};

	// Клік поза межами розділу "Кошик" для його закриття
	if (!targetElement.closest('.cart')) {
		document.querySelector('.cart-wrapper').style.display = 'none';
	};
	// Клік по посиланню "Кошик"
	if (targetElement.closest('.cart__icon-link')) {
		event.preventDefault();
		document.querySelector('.cart-wrapper').style.display = 'flex';
	};
	// Клік по іконці "Видалити" в кошику для видалення товару з кошика
	if (targetElement.classList.contains('delete-product-button')) {
		event.preventDefault();
		if (targetElement.closest('.cart__product-item')) {
			targetElement.closest('.cart__product-item').remove();
			totalSumCart();
			updateCart();
			totalCountProductsInCart();
		};
	};
	// Клік по кнопці "Замовити" в кошику
	if (targetElement.classList.contains('cart__button')) {
		event.preventDefault();
		sendOrder();
	};
	// Клік по картинці товару
	if (targetElement.closest('.product-card__img')) {
		const pId = targetElement.closest('.product-card').dataset.prodid;
		getPhotoProduct(pId);
	};
	// Закриття відкритого товару
	if (!targetElement.closest('.product')) {
		if (targetElement.closest('.product-wrapper')) {
			targetElement.closest('.product-wrapper').remove();
		};
	};
	// Клік по іконці для додавання товару в вибране
	if (targetElement.closest('.product-card__favorite')) {
		event.preventDefault();
		const productId = targetElement.closest('.product-card').dataset.prodid;
		productInFavorite(productId);
		changeIconLike(productId);
	};
	// Клік поза межами розділу "Вибране" для його закриття
	if (!targetElement.closest('.favorite')) {
		document.querySelector('.favorite-wrapper').style.display = 'none';
	};
	// Клік по посиланню "Вибране"
	if (targetElement.closest('.favorite__icon-link')) {
		event.preventDefault();
		document.querySelector('.favorite-wrapper').style.display = 'flex';
	};
	// Клік по товару в "Вибране"
	if (targetElement.closest('.favorite__img-product')) {
		const pId = targetElement.closest('.favorite__product-item').dataset.prodid;
		getPhotoProduct(pId);
	};
	// Клік по товару в корзині
	if (targetElement.closest('.cart__img-product')) {
		const pId = targetElement.closest('.cart__product-item').dataset.prodid;
		getPhotoProduct(pId);
	};
	// Клік по хрестику для закриття вікна
	if (targetElement.closest('.close')) {
		if (targetElement.closest('.product-wrapper')) {
			targetElement.closest('.product-wrapper').remove();
		};
		if (targetElement.closest('.cart-wrapper')) {
			document.querySelector('.cart-wrapper').style.display = 'none';
		};
		if (targetElement.closest('.favorite-wrapper')) {
			document.querySelector('.favorite-wrapper').style.display = 'none';
		};
		// Клік по іконці "Видалити" в "Вбране" для видалення товару з "Вибраного"
		if (targetElement.closest('.favorite__product-item')) {
			const prodId = targetElement.closest('.favorite__product-item').dataset.prodid
			document.querySelector('.favorite-wrapper').style.display = 'flex';
			removeProductInFavorite(prodId);
			changeIconLike(prodId);
			updateIconFavorite();
		};
		if (targetElement.closest('.contacts-wrapper')) {
			document.querySelector('.contacts-wrapper').style.display = 'none';
		};
		if (targetElement.closest('.for-me-wrapper')) {
			document.querySelector('.for-me-wrapper').style.display = 'none';
		};
		if (targetElement.closest('.product-wrapper')) {
			targetElement.closest('.product-wrapper').remove();
		};
		if (targetElement.closest('.account-wrapper')) {
			document.querySelector('.account-wrapper').style.display = 'none';
			document.querySelector('.account__login-form').style.display = 'none';
			document.querySelector('.account__register-form').style.display = 'none';
		};
	}
};